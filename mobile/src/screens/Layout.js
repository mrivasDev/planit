import React from 'react'
import { BottomNavigation, Text } from 'react-native-paper'
import Dashboard from './Dashboard'
import Profile from './Profile'

const DashboardRoute = () => <Dashboard />

const AlbumsRoute = () => <Text>Albums</Text>

const ProfileRoute = () => <Profile />

export default function Layout({ navigation }) {
  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([
    { key: 'dashboard', title: 'Dashboard', icon: 'album' },
    { key: 'albums', title: 'Albums', icon: 'album' },
    { key: 'profile', title: 'Profile', icon: 'account' },
  ])

  const renderScene = BottomNavigation.SceneMap({
    dashboard: DashboardRoute,
    albums: AlbumsRoute,
    profile: ProfileRoute,
  })

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  )
}
